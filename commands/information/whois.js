const { MessageEmbed } = require('discord.js')
const { getMember, formatDate } = require('../../functions')
const { stripIndents } = require('common-tags')

module.exports.run = async (Bot, message, args) => {
    message.delete()
    const member = getMember(message, args.join(" "));

    //Member Variables
    const joined = formatDate(member.joinedAt);
    const roles = member.roles.cache
        .filter(r => r.id !== message.guild.id)
        .map(r => r)
        .join(" ") || "none";

    //User Variables
    const created = formatDate(member.user.createdAt);

    const wEmbed = new MessageEmbed()
        .setFooter(member.displayName, member.user.displayAvatarURL())
        .setThumbnail(member.user.displayAvatarURL())
        .setColor("#5378c8")

        .addField(`__**Member information**__`, stripIndents`**>Display Name:** ${member.displayName}
        **>Joined At:** ${joined}
        **>Roles:** ${roles}`, true)

        .addField('__**User information**__', stripIndents`**>ID:** ${member.user.id}
        **>Username:** ${member.user.username}
        **>Discord Tag:** ${member.user.tag}
        **>Created At:** ${created}`, true)

        .setTimestamp()

    if (member.user.presence.game)
        wEmbed.addField(`Currently playing`, `**> Name:** ${member.user.presence.game.name}`)

    message.channel.send(wEmbed);
}
module.exports.config = {
    name: 'whois',
    aliases: ["ui", "mi"],
    description: "Tells you about member",
    usage: "=whois (member)",
    category: "information",
    accessableby: "Members"
}