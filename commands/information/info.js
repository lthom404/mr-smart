const { MessageEmbed } = require("discord.js")


module.exports = {
    config: {
        name: "info",
        description: "Tells You About The Bot.",
        usage: `=info (version | creator | packages)`,
        accessableby: "Members",
        aliases: ["info", "bi"],
        category: "information"
    },
    run: async (bot, message, args) => {
        message.delete();

        if (!message.author.id === "596345705740763137") {
            message.channel.send("🚧 \*\*This Command Is Under Construction\*\* 🚧")
        }

        if (args[0] === "version") {
            const vEmbed = new MessageEmbed()
                .setTitle(`${bot.user.username} Version 📃`)
                .setDescription(`This tells you the version of ${bot.user.username}`)
                .setThumbnail(bot.user.displayAvatarURL())
                .setColor("#5378c8")
                .addField("**Version:**", "1.0.0")
                .setFooter(`${bot.user.username} | Version 📃`)
            message.channel.send(vEmbed).then(m => m.delete({timeout: 15000}))
        }
        if (args[0] === "creator") {
            const cEmbed = new MessageEmbed()
                .setTitle(`${bot.user.username} Creator 🛠`)
                .setDescription(`This tells you the creator of ${bot.user.username}`)
                .setThumbnail(bot.user.displayAvatarURL())
                .setColor("#5378c8")
                .addField("**Creator:**", "<@!596345705740763137>")
                .setFooter(`${bot.user.username} | Creator 🛠`)
            message.channel.send(cEmbed).then(m => m.delete({timeout: 15000}))
        }
        if (args[0] === "packages") {
            const pEmbed = new MessageEmbed()
                .setTitle(`${bot.user.username} Packages 📦`)
                .setThumbnail(bot.user.displayAvatarURL())
                .setColor("#5378c8")
                .setDescription(`\*\*\*These are the Packages for ${bot.user.username}\*\*\*\nbeautify | \`npm i beautify\` | \`0.0.8\`\ncommon-tags | \`npm i common-tags\` | \`1.8.0\`\ndiscord.js | \`npm i discord.js\` | \`^12.2.0\`\nendb | \`npm i endb\` \`^0.22.9\`\nr6api.js | \`npm i r6api.js\` | \`^1.3.0\``)
                .setFooter(`${bot.user.username} | Packages 📦`)
            message.channel.send(pEmbed).then(m => m.delete({timeout: 15000}))
        }
        
        
    }
}