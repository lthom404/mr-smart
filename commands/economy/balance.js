const Discord = require("discord.js");
const colored = ["#da7272", "#da8f72", "#daab72", "#dac272", "#dad872", "#c0da72", "#a9da72", "#8cda72", "#72da75", "#72da95", "#72dab6", "#72dacc", "#72d5da", "#72b6da", "#7296da", "#7274da", "#9172da", "#ab72da", "#bd72da", "#d172da", "#da72c5", "#da72ab", "#da7298"];
const fs = require("fs");
const money = require("../../money.json");

module.exports = { 
    config: {
        name: "balance",
        description: "Check balance!",
        usage: `=balance <author | mention>`,
        category: "economy",
        accessableby: "Members",
        aliases: ["wallet", "bank"]
    },
	run: async (bot, message, args) => {
    
        if (message.channel.type == "dm") {
            const embed = new Discord.MessageEmbed()
            .setColor("#5378c8")
            .setTitle("<a:error:707045703003668521> Forbidden")
            .setDescription("This command is disabled through dm.");
    
        return message.channel.send(embed);
        }

            if(!args[0]) {
                var user = message.author;
            } else {
                var user = message.mentions.users.first() || bot.users.cache.get(args[0]);
            }
    
            if(!money[user.id]) {
                money[user.id] = {
                    name: user.tag,
                    money: 0
                }
                fs.writeFile("./money.json", JSON.stringify(money), (err) => {
                    if(err) console.log(err);
                });
            }
    
            const embed = new Discord.MessageEmbed()
                .setColor("#5378c8")
                .setTitle(`${user.username}'s Balance`)
                .setDescription(`<:coin:706983976375418941> Balance: **$${money[user.id].money}**`)
                .setTimestamp()
                .setFooter(`${message.author.username}`, message.author.avatarURL({ dynamic: true, format: 'png' }));
    
            return message.channel.send(embed);
	}
}
