const Discord = require("discord.js");
const colored = ["#da7272", "#da8f72", "#daab72", "#dac272", "#dad872", "#c0da72", "#a9da72", "#8cda72", "#72da75", "#72da95", "#72dab6", "#72dacc", "#72d5da", "#72b6da", "#7296da", "#7274da", "#9172da", "#ab72da", "#bd72da", "#d172da", "#da72c5", "#da72ab", "#da7298"];
const fs = require("fs");
const money = require("../../money.json");
const ms = require("parse-ms");
const hourlydowns = require("../../hourlydowns.json");

module.exports = {
    config: {
        name: "hr",
        description: "<:coin:706983976375418941> Collect your hourly reward.",
        usage: `=hr`,
        accessableby: "Members",
        aliases: ["hour", "hourly"],
        category: "economy",
    },
    run: async (bot, message, args) => {
    
    if (message.channel.type == "dm") {
        const embed = new Discord.MessageEmbed()
        .setColor(colored[~~(Math.random() * colored.length)])
        .setTitle("<a:error:707045703003668521> Forbidden")
        .setDescription("This command is disabled through dm.");

    return message.channel.send(embed);
    }

        let timeout = 3.6e+6; 
        let reward = 250;

        let embed = new Discord.MessageEmbed()
        embed.setTitle("Hourly Reward");

        if(!money[message.author.id]) {

            money[message.author.id] = {
                name: bot.users.cache.get(message.author.id).tag,
                money: reward
            }
            fs.writeFile("./money.json", JSON.stringify(money), (err) => {
                if(err) console.log(err);
            });

            if(!hourlydowns[message.author.id]) {
                hourlydowns[message.author.id] = {
                    name: bot.users.cache.get(message.author.id).tag,
                    hourly: Date.now()
                }
                fs.writeFile("./hourlydowns.json", JSON.stringify(hourlydowns), (err) => {
                    if(err) console.log(err);
                });
            } else {
                hourlydowns[message.author.id].hourly = Date.now()
                fs.writeFile("./hourlydowns.json", JSON.stringify(hourlydowns), (err) => {
                    if(err) console.log(err);
                });
            }

            embed.setDescription(`<:coin:706983976375418941> You collected your hourly reward of **$${reward}**.\nYour updated balance is, **$${money[message.author.id].money}**.`);
            embed.setColor(colored[~~(Math.random() * colored.length)]);
            embed.setTimestamp();
            embed.setFooter(`Requested by ${message.author.username}`, message.author.avatarURL({ dynamic: true, format: 'png' }));
            return message.channel.send(embed);

        } else {

            if(!hourlydowns[message.author.id]) {
                hourlydowns[message.author.id] = {
                    name: bot.users.cache.get(message.author.id).tag,
                    hourly: Date.now()
                }
                fs.writeFile("./hourlydowns.json", JSON.stringify(hourlydowns), (err) => {
                    if(err) console.log(err);
                });

                money[message.author.id].money += reward;
                fs.writeFile("./money.json", JSON.stringify(money), (err) => {
                    if(err) console.log(err);
                });

                embed.setDescription(`<:coin:706983976375418941> You collected your hourly reward of **$${reward}**.\nYour updated balance is, **$${money[message.author.id].money}**.`);
                embed.setColor(colored[~~(Math.random() * colored.length)]);
                embed.setTimestamp();
                embed.setFooter(`Requested by ${message.author.username}`, message.author.avatarURL({ dynamic: true, format: 'png' }));
                return message.channel.send(embed);

            } else {
                
                if(timeout - (Date.now() - hourlydowns[message.author.id].hourly) > 0) {

                    let time = ms(timeout - (Date.now() - hourlydowns[message.author.id].hourly));

                    embed.setColor(colored[~~(Math.random() * colored.length)]);
                    embed.setTitle("🕰️ Cooldown in effect");
                    embed.setDescription(`You can collect again in, **${time.minutes}m ${time.seconds}s**`);
                    embed.setTimestamp();
                    embed.setFooter(`Requested by ${message.author.username}`, message.author.avatarURL({ dynamic: true, format: 'png' }));
                    return message.channel.send(embed);
                    
                } else {

                    money[message.author.id].money += reward;
                    fs.writeFile("./money.json", JSON.stringify(money), (err) => {
                        if(err) console.log(err);
                    });

                    hourlydowns[message.author.id].hourly = Date.now();
                    fs.writeFile("./hourlydowns.json", JSON.stringify(hourlydowns), (err) => {
                        if(err) console.log(err)
                    });

                    embed.setDescription(`<:coin:706983976375418941> You collected hourly reward of **$${reward}**.\nYour updated balance is, **$${money[message.author.id].money}**.`);
                    embed.setColor(colored[~~(Math.random() * colored.length)])
                    embed.setTimestamp();
                    embed.setFooter(`Requested by ${message.author.username}`, message.author.avatarURL({ dynamic: true, format: 'png' }));
                    return message.channel.send(embed);

                }

            }

        }

    }
    
}