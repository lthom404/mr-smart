const { MessageEmbed } = require('discord.js');

module.exports = {
    config: {
        name: "dm",
        description: "Send Direct Message To A Member Of Your Choice",
        usage: `-dm <@user> <input>`,
        accessableby: "Members",
        aliases: [""],
        category: "miscellanous"
    },
    run: async (bot, message, args) => {
        message.delete();
        let member = await message.mentions.users.first() || await bot.users.fetch(args[0]).catch(() => null) || await message.guild.members.cache.get(args[0]);
        if (!member || !args[0]) return message.reply('please provide a valid user.').then(m => m.delete({timeout: 5000}));
        let msg = args.slice(1).join(' ') || 'No message provided';

        try {
            member.send(
                new MessageEmbed()
                    .setColor('#5378c8K')
                    .setAuthor(`Direct Message | ${message.guild.name}`, bot.user.displayAvatarURL())
                    .setDescription(msg.size > 1900 ? `${msg.substr(0, 1900)}...` : msg)
            )
            const embed = new MessageEmbed()
            .setTitle(`✔ Successfully sent a direct message to: \`${member.tag}\``)        
            .setColor('#5378c8K')
            return message.channel.send(embed).then(m => m.delete({timeout: 5000}));
        } catch (err) {
            console.error(err);
            return message.reply('❌ Couldnt dm that user.').then(m => m.delete({timeout: 5000}));
        }
    }
}