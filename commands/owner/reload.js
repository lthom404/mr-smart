const Discord = require("discord.js")
const botconfig = require("../../botconfig.json")
const { readdirSync } = require('fs'); 
const {join} = require('path'); 

module.exports.run = async (bot, message, args) => {
    message.delete()
    if (message.author.id != "596345705740763137") return message.channel.send("You are not the bot owner!").then(m => m.delete({ timeout: 15000 }))
    if (!args[0]) return message.channel.send("Please provide a command to reload!").then(m => m.delete({ timeout: 15000 }))
    const commandName = args[0].toLowerCase()

    if (!bot.commands.get(commandName)) return message.channel.send("That command doesn't exist. Try again.").then(m => m.delete({timeout: 5000}))
    readdirSync(join(__dirname, '..')).forEach(f => {

        let files = readdirSync(join(__dirname, '..', f));

        if (files.includes(commandName + '.js')) {
            try {
                delete require.cache[require.resolve(`../${f}/${commandName}.js`)] // usage !reload <name>
                bot.commands.delete(commandName)
                const pull = require(`../${f}/${commandName}.js`)
                bot.commands.set(commandName, pull)
                const sEmbed = new Discord.MessageEmbed()
                .setTitle(`✔ Successfully reloaded ${commandName}.js!`)
                .setColor("#5378c8K")
                return message.channel.send(sEmbed).then(m => m.delete({timeout: 5000}))
            } catch (e) {
                return message.channel.send(`Could not reload: \`${args[0].toUpperCase()}\``).then(m => m.delete({timeout: 5000}))
            }
        }
    })
}
module.exports.config = {
    name: "reload",
    aliases: ["creload"],
    description: "Reloads a command",
    usage: "=reload",
    accessableby: "Bot Owner",
    category: "owner"
}