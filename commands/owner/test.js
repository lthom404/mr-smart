const Discord = require ('discord.js')
const ownerid = '596345705740763137'

module.exports = {
	
	config: {
		name: 'Test',
		description: 'Tests if the bot is sending normal messages',
		usage: '=test <normal | embed>',
		category: 'owner',
		aliases: ['test']
	}, 
	run: async ( bot , message , args ) => {
		message.delete()
		
		if(!message.author.id == ownerid) {
			return message.channel.send("You are not the bot owner").then(m => m.delete({timeout: 5000}))
		}
		if( !args[0] ) {
			return message.channel.send("usage: \`=test <normal | embed>\`").then(m => m.delete({timeout: 5000}))
		}
		if ( args[0] == "normal") {
			return message.channel.send("This Is A Test Message").then(m => m.delete({timeout: 5000}))
		}
		if ( args[0] == "embed" ) {
			const tEmbed = new Discord.MessageEmbed()
				.setTitle(`${bot.user.username} Test Title`)
				.setDescription("This Is A Test Description")
				.setThumbnail(bot.user.displayAvatarURL())
				.addField("Test Field Title", "This Is A Test Field")
				.setFooter("This Is A Test Footer", )
				.setTimestamp()
			message.channel.send(tEmbed).then(m => m.delete({timeout: 5000}))
		}
	}
}