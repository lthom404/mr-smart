const Discord = require('discord.js')
const { stripIndents } = require('common-tags')
const { promptMessage } = require('../../functions')

module.exports = {
    config: {
        name: "ban",
        aliases: ["bannish", "begone"],
        description: "Bans a member",
        usages: "=ban <@member> <reason> ",
        category: "moderation"
    },
    run: async (Bot, message, args) => {
        message.delete();
        const logChannel = message.guild.channels.cache.find(c => c.name === 'modlogs') || message.channel;
        //No args
        if (!args[0]) {
            return message.reply('Please provide a member to ban')
                .then(m => m.delete({ timeout: 5000 }));
        }
        if (!args[1]) {
            return message.reply('Please provide reason to ban')
                .then(m => m.delete({ timeout: 5000 }));
        }
        if (!message.member.hasPermission("BAN_MEMBERS")) {
            return message.reply("❌ You do not have permissions to ban members. Please contact a staff member!")
                .then(m => m.delete({ timeout: 5000 }));
        }

        if (!message.guild.me.hasPermission("BAN_MEMBERS")) {
            return message.reply("❌ You do not have permissions to ban members. Please contact a staff member!")
                .then(m => m.delete({ timeout: 5000 }));
        }

        const toBan = message.mentions.members.first() || message.guild.members.cache.get(args[0]);

        if (!toBan) {
            return message.reply("Couldnt find that member, try again!")
                .then(m => m.delete({ timeout: 5000 }));
        }
        if (message.author.id === toBan.id) {
            return message.reply("Cant ban yourself!")
                .then(m => m.delete({ timeout: 5000 }));
        }

        if (!toBan.bannable) {
            return message.reply("I cant ban this member because of role hierachy, I suppose")
                .then(m => m.delete({ timeout: 5000 }));
        }

        const bEmbed = new Discord.MessageEmbed()
        .setTitle("\*\*\*Member Has Been Banned 🔨\*\*\*")    
        .setColor("#ff0000")
            .setThumbnail(toBan.user.displayAvatarURL())
            .setFooter(message.author.username, message.author.displayAvatarURL())
            .setTimestamp()
            .setDescription(stripIndents`**>Banned Member:** ${toBan} (${toBan.id})
        **>Banned By:** ${message.author} (${message.author.id})
        **>Reason:** ${args.slice(1).join(" ")}`)

        const promptEmbed = new Discord.MessageEmbed()
            .setColor("GREEN")
            .setAuthor("This verification becomes invalid after 30s")
            .setDescription(`Are you sure you want to ban ${toBan}?`);

        await message.channel.send(promptEmbed).then(async msg => {
            const emoji = await promptMessage(msg, message.author, 30, ["✔", "❌"]);

            if (emoji === "✔") {
                msg.delete();

                toBan.ban(args.slice(1).join(" "))
                    .catch(err => {
                        if (err) return message.channel.send(`Well..... something went wrong?`);
                    });

                logChannel.send(bEmbed);
            } else if (emoji === "❌") {
                msg.delete();

                message.reply("Ban Cancelled")
                    .then(m => m.delete({ timeout: 5000 }));
            }
        });
    }
}

